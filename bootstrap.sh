#!/usr/bin/env bash

#Do initial update and install additional libraries for processing
apt-get -y update
apt-get -y install python-software-properties

#Install php 5.6 with additional libraries (which also installs apache2)
apt-get -y install php5 libapache2-mod-php5 php5-mcrypt
apt-get -y install php-pear
apt-get -y install php5-dev

#Installs the mongo driver for php
echo "no" > answers.txt
sudo pecl install mongo < answers.txt
rm answers.txt
cp /vagrant/setup/php.ini /etc/php5/apache2/.

# Add key and get repo info for mongodb
apt-get -y update
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list

#Update aptitude and install mongodb
apt-get -y update
apt-get install -y mongodb-org

#Wipes www dir and links the shared folders to the host
rm -rf /var/www
ln -fs /vagrant/served /var/www

#Copies over configs for the API and main site
cp /vagrant/setup/boogle.dev.conf /etc/apache2/sites-available/boogle.dev.conf
a2dissite 000-default
service apache2 reload
a2ensite boogle.dev.conf
service apache2 reload

#Sets up mongo db
mongoimport -d boogle -c movies --type csv --file /vagrant/setup/Movies.csv --headerline