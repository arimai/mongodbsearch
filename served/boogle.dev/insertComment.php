<?php
/**
 * Created by PhpStorm.
 * User: ira
 * Date: 5/3/15
 * Time: 7:03 PM
 */

require_once "db.php";
global $mongoObject;
$bool=false;
if(!empty($_POST['content'])){
    $bool=$mongoObject->insertComment($_GET['oid'],$_POST['content'],$_POST['author']);
}
if($bool) {
    header("Location: movie.php?comment=added&oid=" . $_GET['oid']);
}
else{
    header("Location: movie.php?comment=blank&oid=" . $_GET['oid']);
}
