<?php
/**
 * Created by PhpStorm.
 * User: ira
 * Date: 5/3/15
 * Time: 11:17 AM
 */

require_once "db.php";
global $mongoObject;
    //echo extension_loaded("mongo")?"Mongo Driver Loaded\n":"MongoDriver NOT Loaded/n";

    $search=$_REQUEST['searchString'];
    $item=$mongoObject->find_query("title","regex",$search);
    if(!empty($item)) {
        foreach ($item as $res) {
            echo "<div class='row search_row'>";
            echo "<div class='thumbnail'>";
            echo "<div class='caption'>";
            echo "<h3 class='text-primary'><a href='movie.php?oid={$res['_id']}' >{$res['title']}</a></h3>";
            echo "<p class='genre'>" . $res['genres'] . "</p>";
            echo "<h4 class='rating'><mark> RATING : " . $res['rating'] . "</mark></h4>";
            if(!empty($res['comments'])){
                foreach($res['comments'] as $comment){
                    echo "<blockquote>
                          <p>{$comment['content']}</p>
                          <footer>{$comment['author']}</footer>
                            </blockquote>";
                }
            }
            echo "</div></div></div>";
        }
    }
    else{
        echo "<div class=row search_row>";
        echo "<p>No results found</p>";
        echo "</div>";
    }



