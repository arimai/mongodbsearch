<?php
/**
 * Created by PhpStorm.
 * User: ira
 * Date: 5/3/15
 * Time: 3:58 PM
 */
class db{

    private $m;
    private $db;
    private $coll;
    public $item=array();

    function __construct(){
	    $this->m = new MongoClient("mongodb://localhost");
	     $this->db = $this->m->boogle;
	     $this->coll = $this->db->movies;
    }

    function find_query($field,$searchType,$value){
        if($searchType=='id') {
            $item = $this->coll->find(array($field => new MongoId($value)));
            $this->item=$item;
            return $item;
        }
        else{
            $item = $this->coll->find(array($field=> new MongoRegex("/^$value/i")));
            $this->item=$item;
            return $item;
        }

    }

    function insertComment($id,$content,$author){
        $bool=$this->coll->update(array("_id"=>new MongoId($id)),array(
            '$addToSet' => array("comments" => array('content' => $content, 'author' => $author)))
        );
        if($bool)
        {return true;}
    }

    function field($fieldname){
        if(isset($this->item)){
            foreach($this->item as $res){
                return $res[$fieldname];
            }
        }
    }
}
$mongoObject=new db();
