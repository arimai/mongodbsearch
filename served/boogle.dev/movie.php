<?php
/**
 * Created by PhpStorm.
 * User: ira
 * Date: 5/3/15
 * Time: 2:19 PM
 */
require_once "db.php";
global $mongoObject;
if(isset($_GET['oid'])){
    echo "<br />";
    $item=$mongoObject->find_query("_id","id",$_GET['oid']);
}
if(isset($_GET['comment'])){
    if($_GET['comment']=='added'){
        echo "<div class='alert alert-info'>";
        echo "<strong>Your comment was added to our database</strong>";
        echo "</div>";
    }
    elseif($_GET['comment']=='blank'){
        echo "<div class='alert alert-danger'>";
        echo "<strong>You need to type something in the content box in order to comment!</strong>";
        echo "</div>";
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Boogle</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <script>
        function comment(){
            var commentForm=document.createElement("form");
            var textbox="<div class='form-group'><label for='comment'>Comment:</label> <textarea class='form-control' rows='5' name='content'></textarea></div>";
            textbox+="<div class='form-group'><label for='author'>Author&nbsp;</label><input type='text' name='author' />";
            textbox+="&nbsp;&nbsp;<input class='btn btn-primary' type='submit' role='button' value='Comment'></div>";
            textbox+="<div class='row space text-center'><a href='index.php' class='btn btn-large btn-default' role='button'>&lt;&lt;&nbsp;Make another search</a></div>";
            commentForm.innerHTML=textbox;
            commentForm.setAttribute("action","insertComment.php?oid=<?php if(isset($_GET['oid'])){echo $_GET['oid'];}?>");
            commentForm.setAttribute("method","POST");
            document.getElementById("area").appendChild(commentForm);
        }
    </script>
</head>
<body>
<div class="container ">
    <div class="row ">
        <a href="index.php" class="btn btn-large back" role="button">&lt;&lt;&nbsp;Make another search </a>
    </div>

    <div class="row ">
    <div class="col-xs-6 col-sm-5 movie">
        <img class="img movie_images" src="<?php echo $mongoObject->field('imageSrc')?>">
    </div>
    <div class="col-xs-6 col-md-7 t">
        <div class="row">
            <div class="text-center">
                <div id="area" class="caption">
                    <h1 class="text-primary"><?php echo $mongoObject->field('title') ?></h1>
                    <br />
                    <p class="text-danger rating">Movie ID : <?php echo $mongoObject->field('movieId')?></p>
                    <br />
                    <p class="text-success genre"><mark>GENRE : <?php echo $mongoObject->field('genres') ?></mark></p>
                    <br /><br />
                    <p align="justify" class="text-default summary"><?php echo $mongoObject->field('summary') ?></p>
                    <br />
                    <p class="text-primary rating"><mark>RATING : <?php echo $mongoObject->field('rating') ?></mark></p>
                    <br />
                    <p> <button onclick="comment()" class="btn btn-danger">LEAVE COMMENTS</button></p>
                </div>
            </div>
        </div>

    </div>

    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>



