<?php
/**
 * Created by PhpStorm.
 * User: ira
 * Date: 5/1/15
 * Time: 7:35 PM
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Boogle</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
    <script>
        function showPreview(str){
            if(str.length==0){
                document.getElementById("testing").innerHTML="";
                return;
            }
            else{
                var xmlhttp=new XMLHttpRequest();
                xmlhttp.onreadystatechange=function(){
                    if(xmlhttp.readyState==4 && xmlhttp.status==200){
                        document.getElementById("testing").innerHTML=xmlhttp.responseText;
                    }
                }
            xmlhttp.open("GET","search.php?searchString="+str,true);
            xmlhttp.send();
            }
        }

    </script>
</head>

<body>
<section>
    <div class="container">
        <br /><br />
        <div class="row">
            <img src="./images/boogle.gif" class="img img-responsive img1" />
        </div>
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <form class="form" action="index.php" method="POST">
                <div class="f1 input-group">
                    <input type="text" id="s1" name="search" class="form-control" onkeyup="showPreview(this.value)" placeholder="Type Here" aria-describedby="basic-addon2"
                        value="<?php if(isset($_POST['search'])){echo $_POST['search'];}?>">
                    <span class="input-group-addon">Enter a Movie name</span>
                </div>
                </form>
            </div>
        </div>
        <br />
        <div class="row" id="testing">

        </div>
    </div>
</section><!-- layout -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        <?php
         if(isset($_POST['search'])) {
            echo "showPreview('".$_POST['search']."');";
         }
        ?>
    });
</script>
</body>
</html>
